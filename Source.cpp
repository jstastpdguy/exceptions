#include <iostream>
#include <exception>

class Fraction
{
	int numerator;
	int denominator;
public:
	Fraction(int _numerator, int _denominator) : numerator(_numerator), denominator(_denominator)
	{
		if( !_denominator ) throw std::runtime_error( "denominator cannot be equal 0");
	}
};

int main()
{
	int numerator, denominator;

	try{
	
		std::cout << "enter numerator: ";
		std::cin >> numerator;

		if(numerator > 9 || numerator < -9)
		{
			throw std::exception("numerator out of range\n");
		}
		
		std::cout << "enter denominator: ";
		std::cin >> denominator;

		if (denominator > 9 || denominator < -9)
		{
			throw std::exception("denominator out of range\n");
		}
		
		Fraction f(numerator, denominator);
	}
	catch (std::runtime_error err)
	{
		std::cerr << err.what() << '\n';
	}
	catch (std::exception err)
	{
		std::cerr << err.what() << '\n';
	}

	return 0;
}